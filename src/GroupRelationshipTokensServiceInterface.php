<?php

namespace Drupal\group_relationship_tokens;

/**
 * Interface for the group_relationship_tokens.service.
 */
interface GroupRelationshipTokensServiceInterface {

  /**
   * Loads all configured entity_types.
   *
   * @return array
   *   An array of entity_types used by group_relationship_types.
   */
  public function getConfiguredEntityTypes(): array;

}
