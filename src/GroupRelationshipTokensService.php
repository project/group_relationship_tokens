<?php

namespace Drupal\group_relationship_tokens;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * The group_relationship_type.service object.
 */
class GroupRelationshipTokensService implements GroupRelationshipTokensServiceInterface {

  /**
   * Constructs a GroupRelationshipTokensService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(private readonly EntityTypeManagerInterface $entityTypeManager) {}

  /**
   * {@inheritdoc}
   */
  public function getConfiguredEntityTypes(): array {
    $entity_types = [];
    /** @var \Drupal\group\Entity\GroupRelationshipTypeInterface $group_relationship_type */
    foreach ($this->entityTypeManager->getStorage('group_relationship_type')->loadMultiple() as $group_relationship_type) {
      /** @var \Drupal\group\Entity\GroupRelationshipInterface */
      $group_relation = $group_relationship_type->getPlugin();
      $entity_type_id = $group_relation->getPluginDefinition()->getEntityTypeId();
      if (!in_array($entity_type_id, $entity_types)) {
        $storage = $this->entityTypeManager->getStorage($entity_type_id);
        $entity_types[$entity_type_id] = $storage->getEntityType();
      }
    }

    return $entity_types;
  }

}
