<?php

/**
 * @file
 * Builds placeholder replacement tokens for group_relationship data.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\group\Entity\GroupRelationshipInterface;

/**
 * Implements hook_token_info().
 *
 * Generate info to be able to select these tokens from browser and pass
 * validation on alias pattern edit form.
 */
function group_relationship_tokens_token_info() {
  $info = [];

  /** @var \Drupal\group_relationship_tokens\GroupRelationshipTokensServiceInterface $group_relationship_type_service */
  $group_relationship_type_service = \Drupal::service('group_relationship_tokens.service');
  foreach ($group_relationship_type_service->getConfiguredEntityTypes() as $type => $data) {
    $info['tokens']['group_relationship'][$type] = [
      'name' => $data->getLabel(),
      'type' => $type,
    ];
  }

  return $info;
}

/**
 * Implements hook_tokens().
 */
function group_relationship_tokens_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  // Automatically expose related entities to group_relationships.
  if ($type == 'group_relationship' && !empty($data[$type])) {
    $token_service = \Drupal::token();

    /** @var \Drupal\group_relationship_tokens\GroupRelationshipTokensServiceInterface $group_relationship_type_service */
    $group_relationship_type_service = \Drupal::service('group_relationship_tokens.service');

    $group_relationship = $data['group_relationship'];
    assert($group_relationship instanceof GroupRelationshipInterface);

    foreach ($tokens as $name => $original) {
      /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type */
      foreach ($group_relationship_type_service->getConfiguredEntityTypes() as $entity_type_id => $entity_type) {
        if ($name == $entity_type_id) {
          $entity = $group_relationship->getEntity();
          $bubbleable_metadata->addCacheableDependency($entity);
          $replacements[$original] = $entity->label();
        }

        // Actual chaining of tokens handled below.
        if ($entity_tokens = $token_service->findWithPrefix($tokens, $entity_type_id)) {
          $entity = $group_relationship->getEntity();
          $lang = $group_relationship->language()->getId();
          $entity = $entity->hasTranslation($lang) ? $entity->getTranslation($lang) : $entity;
          $replacements += $token_service->generate($entity_type_id, $entity_tokens, [$entity_type_id => $entity], $options, $bubbleable_metadata);
        }
      }
    }
  }

  return $replacements;
}
