CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended Modules
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The Group relationship tokens module adds support for tokens of entities
referenced by group relationships. Out of the box there are already plenty of
tokens available from the group module itself. In case those don't fit your
needs, you can chain the group_relationship's entity tokens to get the desired
value from the referenced entity.

This can for instance be useful if you want to use data from the referenced
entity to build a pathauto pattern.


REQUIREMENTS
------------

This module requires the following module:

* Group - https://www.drupal.org/project/group
* Tokens - https://www.drupal.org/project/tokens


RECOMMENDED MODULES
-------------------

* Pathauto - https://www.drupal.org/project/pathauto


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

There is no configuration needed. The tokens are exposed to your token browser
automatically.  For every entity_type your group_relationship configurations are
connected to, a new token will be exposed.

For the gnode module you could use a token like that to get the node's title:
```
[group_relationship:node:title]
```

To get the value of a custom text field on the node, you could use something
like that:
```
[group_relationship:node:field_custom:value]
```

This also works for users on a membership group_relationship:
```
[group_relationship:user:name]
```

MAINTAINERS
-----------

Current maintainers:

* Pascal Crott - https://www.drupal.org/u/hydra
